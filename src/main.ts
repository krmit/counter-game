#!/usr/bin/env node
import prompt from 'promise-prompt';
import colors from 'colors';
import {Question, question, setupSeed} from './question';

async function main() {
  //const name = prompt("What is your name? ");
 // name.then((name:string)=> {
  //  console.log("Hello "+colors.bold.red(name)+"!!")
  setupSeed(10);
    const start = new Date();
    //const name = await prompt("What is your name? ");
    for(let i = 0; i <5; i++) {
    const my_question = question();
    let answer = Number(await prompt(colors.bold("Addera "+my_question.next+": ")));

    if(answer !== my_question.answer) {
           console.log(colors.bold.red("Du skrev fel!"));
           console.log(colors.red(""+my_question.answer));
           break;
    }
    }

    const end = new Date();
    const diff = Number(end) - Number(start);
    console.log(colors.bold.blue(
         TwoDigit(Math.floor(diff/(60*1000)))
    +":"+TwoDigit(Math.floor((diff/1000)%60))
    +":"+TwoDigit(Math.floor(diff%1000%60/10))));
 //})
}

main();

export function TwoDigit(n:number):string {
  return n>9?String(n):"0"+n;
}
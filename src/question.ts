export interface Question {
    sum: number;
    next: number;
    answer: number;
}

let sum = 0;
let random:()=>number = Math.random;

export function setupSeed(seed:number) {
    random = mulberry32(seed);
}

/**
 * Create a question
 *
 * @returns A Question.
 */
export function question():Question {
    let result: Question;
    const next = randomNumber(1,9);
    const answer = sum+next; 
    result = {
        "sum": sum,
        "next": next,
        "answer": answer 
    }
    sum = answer;
    return result;
}

// By bryc from https://stackoverflow.com/a/47593316
// Using a 32 bits random generator with seed by mulberry
function mulberry32(a:number) {
    return function() {
      let t = a += 0x6D2B79F5;
      t = Math.imul(t ^ t >>> 15, t | 1);
      t ^= t + Math.imul(t ^ t >>> 7, t | 61);
      return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }
}

export function randomNumber(min:number, max:number):number {
    return Math.floor(random()*(max-min+1))+min;
}
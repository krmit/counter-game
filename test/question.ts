import { assert } from "chai";
import {randomNumber, setupSeed} from "../src/question";

describe("Test of randomNumber", ()=> {
    setupSeed(11);
     it("Give correct number if seed, test 1",()=> {
         assert.equal(randomNumber(2,6), 4);
     })

     it("Give correct number if seed, test 2",()=> {
        assert.equal(randomNumber(2,6), 4);
    })

    it("Give correct number if seed, test 3",()=> {
        assert.equal(randomNumber(2,6), 5);
    })

    it("Give correct number if seed, test 4",()=> {
        assert.equal(randomNumber(2,6), 4);
    })

    it("Give correct number if seed, test 5",()=> {
        assert.equal(randomNumber(2,6), 6);
    })

    it("Give correct number if seed, test 6",()=> {
        assert.equal(randomNumber(2,6), 4);
    })

    it("Give correct number if seed, test 7",()=> {
        assert.equal(randomNumber(2,6), 2);
    })
})
# Enkelt räknae program

Överar dig på att addera siffror

## Install


```
$ npm install 
```

## Usage


```

```

## API



### Functions



### Class



## License

GPL, see LICENSE file

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

 * **0.0.1** *{{year}}-??-??* First version published

## Author

**©Magnus Kronnäs 2020**
